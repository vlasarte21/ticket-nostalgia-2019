var express = require('express');

var mysql = require('mysql');
const dotenv = require('dotenv');
dotenv.config();

var app = express();

app.get('/album/:album_id', function(req, res){

  var album_id = req.params.album_id;
  var con = mysql.createConnection({
    host: "nostalgia-hackathon.cyruwsjgqfbl.us-east-1.rds.amazonaws.com",
    user: "nostalgia",
    password: "nostalgia",
    database: "nostalgia_hackathon"
  });

  con.connect(function(err) {
    if (err) throw err;
    con.query("SELECT * FROM album WHERE id = '" + album_id + "'", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});

app.get('/user-events/:user_id', function(req, res){
  var user_id = req.params.user_id;

  var con = mysql.createConnection({
    host: "nostalgia-hackathon.cyruwsjgqfbl.us-east-1.rds.amazonaws.com",
    user: "nostalgia",
    password: "nostalgia",
    database: "nostalgia_hackathon"
  });

  con.connect(function(err) {
    if (err) throw err;
    con.query("SELECT * FROM ticket WHERE user_id = '" + user_id + "'", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});

// app.post('/test-upload/:album_id/:picture_link', function(req, res) {
//   console.log(req.params);
// });

app.post('/upload-picture/:album_id/:picture_link', function(req, res) {
  var con = mysql.createConnection({
    host: "nostalgia-hackathon.cyruwsjgqfbl.us-east-1.rds.amazonaws.com",
    user: "nostalgia",
    password: "nostalgia",
    database: "nostalgia_hackathon"
  });

  var album_id = req.params.album_id;
  var picture_link = req.params.picture_link;

  con.connect(function(err) {
    if (err) throw err;
    con.query("INSERT INTO pictures (album_id, picture_link) VALUES ('"
      + album_id + "','" + picture_link + "')");
  });

  res.send('Picture uploaded successfully! May take a moment to appear');
});

app.get('/pictures/:album_id', function(req, res){

  var album_id = req.params.album_id;

  var con = mysql.createConnection({
    host: "nostalgia-hackathon.cyruwsjgqfbl.us-east-1.rds.amazonaws.com",
    user: "nostalgia",
    password: "nostalgia",
    database: "nostalgia_hackathon"
  });
 
  con.connect(function(err) {
    if (err) throw err;
    con.query("SELECT * FROM pictures WHERE album_id = '" + album_id + "'", function (err, result, fields) {
      if (err) throw err;
      res.send(result);
    });
  });
});


app.get('/', function (req, res) {
  res.send('Greetings!');
});

app.get('/renderHTML', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
  // console.log(`Your port is ${process.env.PORT}`);
});